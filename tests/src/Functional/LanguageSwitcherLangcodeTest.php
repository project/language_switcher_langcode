<?php

namespace Drupal\Tests\language_switcher_langcode\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the features from language switching langcode.
 *
 * @group language
 */
class LanguageSwitcherLangcodeTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'locale',
    'locale_test',
    'language',
    'block',
    'language_test',
    'menu_ui',
    'language_switcher_langcode',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'starterkit_theme';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create and log in user.
    $admin_user = $this->drupalCreateUser([
      'administer blocks',
      'administer languages',
      'access administration pages',
      'access content',
    ]);
    if ($admin_user) {
      $this->drupalLogin($admin_user);
    }

    // Add language: FR.
    $edit = [
      'predefined_langcode' => 'fr',
    ];
    $this->drupalGet('admin/config/regional/language/add');
    $this->submitForm($edit, 'Add language');

    // Set the native language name.
    $this->container->get('language.config_factory_override')
      ->getOverride('fr', 'language.entity.fr')->set('label', 'français')->save();

    // Enable URL language detection and selection.
    $edit = ['language_interface[enabled][language-url]' => '1'];
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm($edit, 'Save settings');

    // Enable the language switching block.
    $this->drupalPlaceBlock('language_block:' . LanguageInterface::TYPE_INTERFACE, [
      'id' => 'test_language_block',
      // Ensure a 2-byte UTF-8 sequence is in the tested output.
      'label' => $this->randomMachineName(8) . '×',
    ]);
  }

  /**
   * Functional tests for the language switcher block with the lang codes.
   */
  public function testLanguageSwitcherBlock(): void {
    // We run the checks for Anonymous users:
    $this->drupalLogout();

    // Assert that the Link texts are the lang codes:
    $language_switchers = $this->xpath('//div[@id=:id]/ul/li', [':id' => 'block-test-language-block']);
    $labels = [];
    foreach ($language_switchers as $list_item) {
      $link = $list_item->find('xpath', 'a');
      if ($link) {
        $labels[] = $link->getText();
      }
    }

    $this->assertSame(['EN', 'FR'], $labels);
  }

}
